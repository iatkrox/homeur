    $( function() {
        $( "#datepicker" ).datepicker();
      } );


              $('#homeFeatures .owl-carousel').owlCarousel({
                loop: true,
                 dots: true,
                
                 margin: 30,
                 nav: true,
                 navText: [
                  "<i class='fa fa-angle-left'></i>",
                   "<i class='fa fa-angle-right'></i>"
                 ],
                 autoplay: true,
                 autoplayHoverPause: true,
                 responsive: {
                  0: {
                    items: 1,
                    margin: 15
                  },
                   700: {
                     items: 2, 
                     margin : 50
                   },
                   1000: {
                    items: 2,
                    margin: 30
                   },
                   900: {
                    items: 2,
                    margin: 0
                   },
                   1300: {
                    items: 3
                   }
               }
             });





